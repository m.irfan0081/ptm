﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CORSEC.Master" AutoEventWireup="true" CodeBehind="under-construction.aspx.cs" Inherits="Pertamina.CORSEC._2019.under_construction" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">

    <!-- begin:: Content -->
    <div class="kt-content  kt-content--fit-top  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <!-- begin:: Content -->

        <!-- begin:: Hero -->
        <div class="kt-sc" style="background-image: url('<%: ResolveUrl("~/Content/assets/media/bg/bg-9.jpg") %>')">
            <div class="kt-container ">

                <div class="kt-sc__bottom">
                    <h3 class="kt-sc__heading kt-heading kt-heading--center kt-heading--xxl kt-heading--medium">THIS PAGE IS UNDER CONSTRUCTION
                    </h3>

                </div>
            </div>
        </div>
        <!-- end:: Hero -->
        </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
</asp:Content>
