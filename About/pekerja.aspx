﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CORSEC.Master" AutoEventWireup="true" CodeBehind="pekerja.aspx.cs" Inherits="Pertamina.CORSEC._2019.About.pekerja" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">

				<!-- end:: Header -->
				<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

					<!-- begin:: Content Head -->
					<div class="kt-subheader  kt-grid__item" id="kt_subheader">
						<div class="kt-container  kt-container--fluid ">
							<div class="kt-subheader__main">
								<h3 class="kt-subheader__title">Dashboard</h3>
								<span class="kt-subheader__separator kt-subheader__separator--v"></span>
								<a href="#" class="btn btn-label-success btn-bold btn-sm btn-icon-h kt-margin-l-10">
									<i class="fa fa-plus"></i>Tambah Data
								</a>
								<div class="kt-input-icon kt-input-icon--right kt-subheader__search kt-hidden">
									<input type="text" class="form-control" placeholder="Search order..."
										id="generalSearch">
									<span class="kt-input-icon__icon kt-input-icon__icon--right">
										<span><i class="flaticon2-search-1"></i></span>
									</span>
								</div>
							</div>
							<div class="kt-subheader__toolbar">
								<div class="kt-subheader__wrapper">
									<a href="#" class="btn kt-subheader__btn-daterange"
										id="kt_dashboard_daterangepicker" data-toggle="kt-tooltip"
										title="Select dashboard daterange" data-placement="left">
										<span class="kt-subheader__btn-daterange-title"
											id="kt_dashboard_daterangepicker_title">Today</span>&nbsp;
										<span class="kt-subheader__btn-daterange-date"
											id="kt_dashboard_daterangepicker_date">Aug 16</span>
										<i class="flaticon2-calendar-1"></i>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- end:: Content Head -->

				<!-- begin:: Content -->
				<div class="kt-content  kt-content--fit-top  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor"
					id="kt_content_header">

					<!-- begin:: Content -->

					<!-- begin:: Hero -->					
                    <div class="kt-sc" style="background-image: url('<%: ResolveUrl("~/Content/assets/media/bg/bg-9.jpg") %>')">
						<div class="kt-container ">

							<div class="kt-sc__bottom">
								<h3
									class="kt-sc__heading kt-heading kt-heading--center kt-heading--xxl kt-heading--medium">
									Daftar Pekerja / Kontrak
								</h3>

							</div>
						</div>
					</div>
					<!-- end:: Hero -->


					<div class="kt-container ">
						<div class="kt-portlet">
							<div class="kt-portlet__body">
								<div class="kt-infobox">
									<div class="kt-infobox__header">
										<h2 class="kt-infobox__title">Obcaecati nisi laboriosam</h2>
									</div>
									<div class="kt-infobox__body">
										<div class="kt-infobox__section">
											<div class="kt-infobox__content">
												Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus at
												laboriosam
												architecto maiores consequuntur pariatur fuga aperiam labore,
												consectetur ratione ullam,
												accusamus quos optio quibusdam molestias repellendus! Aut nulla dolores
												nisi nemo aperiam et
												quos magni ut officia, nesciunt quia ipsa illo nam quibusdam possimus
												dignissimos ducimus
												veritatis quae tempore amet voluptate repellat. Eos sed est numquam nisi
												hic eveniet quos,
												rerum culpa ipsa,
												<br><br><br><br>

												<h3 class="text-center kt-mb-40 kt-infobox__title">Struktur Jabatan
													Pekerja</h3>

												<div class="row">

													<div class="col-md-6">
														<div class="kt-portlet">
															<div class="kt-portlet__body">
																<!--begin::Accordion-->
																<div class="accordion accordion-solid accordion-toggle-plus"
																	id="accordionExample6">
																	<div class="card">
																		<div class="card-header" id="headingOne6">
																			<div class="card-title"
																				data-toggle="collapse"
																				data-target="#collapseOne6"
																				aria-expanded="true"
																				aria-controls="collapseOne6">
																				<i
																					class="flaticon-safe-shield-protection"></i>
																				Jabatan Pekerja Satu
																			</div>
																		</div>
																		<div id="collapseOne6" class="collapse"
																			aria-labelledby="headingOne6"
																			data-parent="#accordionExample6">
																			<div class="card-body">
																				<div
																					class="kt-widget kt-widget--user-profile-1">
																					<div class="kt-widget__head">
																						<div class="kt-widget__media">
																							<img src="<%: ResolveUrl("~/Content/assets/media/users/100_13.jpg") %>"
																								alt="image">
																						</div>
																						<div class="kt-widget__content">
																							<div
																								class="kt-widget__section">
																								<a href="#"
																									class="kt-widget__username">
																									Jason Muller
																									<i
																										class="flaticon2-correct kt-font-success"></i>
																								</a>
																								<span
																									class="kt-widget__subtitle">
																									Head of Development
																								</span>
																							</div>
																						</div>
																					</div>
																					<div class="kt-widget__body">
																						<div class="kt-widget__content">
																							<div
																								class="kt-widget__info">
																								<span
																									class="kt-widget__label">Email:</span>
																								<a href="#"
																									class="kt-widget__data">matt@fifestudios.com</a>
																							</div>
																							<div
																								class="kt-widget__info">
																								<span
																									class="kt-widget__label">Phone:</span>
																								<a href="#"
																									class="kt-widget__data">44(76)34254578</a>
																							</div>
																							<div
																								class="kt-widget__info">
																								<span
																									class="kt-widget__label">Location:</span>
																								<span
																									class="kt-widget__data">Melbourne</span>
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="card">
																		<div class="card-header" id="headingTwo6">
																			<div class="card-title collapsed"
																				data-toggle="collapse"
																				data-target="#collapseTwo6"
																				aria-expanded="false"
																				aria-controls="collapseTwo6">
																				<i
																					class="flaticon-safe-shield-protection"></i>
																				Jabatan Pekerja Dua
																			</div>
																		</div>
																		<div id="collapseTwo6" class="collapse"
																			aria-labelledby="headingTwo6"
																			data-parent="#accordionExample6">
																			<div class="card-body">
																				<div
																					class="kt-widget kt-widget--user-profile-1">
																					<div class="kt-widget__head">
																						<div class="kt-widget__media">
																							<img src="<%: ResolveUrl("~/Content/assets/media/users/100_13.jpg") %>"
																								alt="image">
																						</div>
																						<div class="kt-widget__content">
																							<div
																								class="kt-widget__section">
																								<a href="#"
																									class="kt-widget__username">
																									Jason Muller
																									<i
																										class="flaticon2-correct kt-font-success"></i>
																								</a>
																								<span
																									class="kt-widget__subtitle">
																									Head of Development
																								</span>
																							</div>
																						</div>
																					</div>
																					<div class="kt-widget__body">
																						<div class="kt-widget__content">
																							<div
																								class="kt-widget__info">
																								<span
																									class="kt-widget__label">Email:</span>
																								<a href="#"
																									class="kt-widget__data">matt@fifestudios.com</a>
																							</div>
																							<div
																								class="kt-widget__info">
																								<span
																									class="kt-widget__label">Phone:</span>
																								<a href="#"
																									class="kt-widget__data">44(76)34254578</a>
																							</div>
																							<div
																								class="kt-widget__info">
																								<span
																									class="kt-widget__label">Location:</span>
																								<span
																									class="kt-widget__data">Melbourne</span>
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="card">
																		<div class="card-header" id="headingThree6">
																			<div class="card-title collapsed"
																				data-toggle="collapse"
																				data-target="#collapseThree6"
																				aria-expanded="false"
																				aria-controls="collapseThree6">
																				<i
																					class="flaticon-safe-shield-protection"></i>
																				Jabatan Pekerja Tiga
																			</div>
																		</div>
																		<div id="collapseThree6" class="collapse"
																			aria-labelledby="headingThree6"
																			data-parent="#accordionExample6">
																			<div class="card-body">
																				<div
																					class="kt-widget kt-widget--user-profile-1">
																					<div class="kt-widget__head">
																						<div class="kt-widget__media">
																							<img src="<%: ResolveUrl("~/Content/assets/media/users/100_13.jpg") %>"
																								alt="image">
																						</div>
																						<div class="kt-widget__content">
																							<div
																								class="kt-widget__section">
																								<a href="#"
																									class="kt-widget__username">
																									Jason Muller
																									<i
																										class="flaticon2-correct kt-font-success"></i>
																								</a>
																								<span
																									class="kt-widget__subtitle">
																									Head of Development
																								</span>
																							</div>
																						</div>
																					</div>
																					<div class="kt-widget__body">
																						<div class="kt-widget__content">
																							<div
																								class="kt-widget__info">
																								<span
																									class="kt-widget__label">Email:</span>
																								<a href="#"
																									class="kt-widget__data">matt@fifestudios.com</a>
																							</div>
																							<div
																								class="kt-widget__info">
																								<span
																									class="kt-widget__label">Phone:</span>
																								<a href="#"
																									class="kt-widget__data">44(76)34254578</a>
																							</div>
																							<div
																								class="kt-widget__info">
																								<span
																									class="kt-widget__label">Location:</span>
																								<span
																									class="kt-widget__data">Melbourne</span>
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<!--end::Accordion-->
															</div>
														</div>
													</div>

													<div class="col-md-6">
														<div class="kt-portlet">
															<div class="kt-portlet__body">
																<!--begin::Accordion-->
																<div class="accordion accordion-solid accordion-toggle-plus"
																	id="accordionExample7">
																	<div class="card">
																		<div class="card-header" id="headingOne7">
																			<div class="card-title collapsed"
																				data-toggle="collapse"
																				data-target="#collapseOne7"
																				aria-expanded="true"
																				aria-controls="collapseOne7">
																				<i
																					class="flaticon-safe-shield-protection"></i>
																				Jabatan Pekerja Empat
																			</div>
																		</div>
																		<div id="collapseOne7" class="collapse"
																			aria-labelledby="headingOne7"
																			data-parent="#accordionExample7">
																			<div class="card-body">
																				<div
																					class="kt-widget kt-widget--user-profile-1">
																					<div class="kt-widget__head">
																						<div class="kt-widget__media">
																							<img src="<%: ResolveUrl("~/Content/assets/media/users/100_13.jpg") %>"
																								alt="image">
																						</div>
																						<div class="kt-widget__content">
																							<div
																								class="kt-widget__section">
																								<a href="#"
																									class="kt-widget__username">
																									Jason Muller
																									<i
																										class="flaticon2-correct kt-font-success"></i>
																								</a>
																								<span
																									class="kt-widget__subtitle">
																									Head of Development
																								</span>
																							</div>
																						</div>
																					</div>
																					<div class="kt-widget__body">
																						<div class="kt-widget__content">
																							<div
																								class="kt-widget__info">
																								<span
																									class="kt-widget__label">Email:</span>
																								<a href="#"
																									class="kt-widget__data">matt@fifestudios.com</a>
																							</div>
																							<div
																								class="kt-widget__info">
																								<span
																									class="kt-widget__label">Phone:</span>
																								<a href="#"
																									class="kt-widget__data">44(76)34254578</a>
																							</div>
																							<div
																								class="kt-widget__info">
																								<span
																									class="kt-widget__label">Location:</span>
																								<span
																									class="kt-widget__data">Melbourne</span>
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="card">
																		<div class="card-header" id="headingTwo7">
																			<div class="card-title collapsed"
																				data-toggle="collapse"
																				data-target="#collapseTwo7"
																				aria-expanded="false"
																				aria-controls="collapseTwo7">
																				<i
																					class="flaticon-safe-shield-protection"></i>
																				Jabatan Pekerja Lima
																			</div>
																		</div>
																		<div id="collapseTwo7" class="collapse"
																			aria-labelledby="headingTwo7"
																			data-parent="#accordionExample7">
																			<div class="card-body">
																				<div
																					class="kt-widget kt-widget--user-profile-1">
																					<div class="kt-widget__head">
																						<div class="kt-widget__media">
																							<img src="<%: ResolveUrl("~/Content/assets/media/users/100_13.jpg") %>"
																								alt="image">
																						</div>
																						<div class="kt-widget__content">
																							<div
																								class="kt-widget__section">
																								<a href="#"
																									class="kt-widget__username">
																									Jason Muller
																									<i
																										class="flaticon2-correct kt-font-success"></i>
																								</a>
																								<span
																									class="kt-widget__subtitle">
																									Head of Development
																								</span>
																							</div>
																						</div>
																					</div>
																					<div class="kt-widget__body">
																						<div class="kt-widget__content">
																							<div
																								class="kt-widget__info">
																								<span
																									class="kt-widget__label">Email:</span>
																								<a href="#"
																									class="kt-widget__data">matt@fifestudios.com</a>
																							</div>
																							<div
																								class="kt-widget__info">
																								<span
																									class="kt-widget__label">Phone:</span>
																								<a href="#"
																									class="kt-widget__data">44(76)34254578</a>
																							</div>
																							<div
																								class="kt-widget__info">
																								<span
																									class="kt-widget__label">Location:</span>
																								<span
																									class="kt-widget__data">Melbourne</span>
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="card">
																		<div class="card-header" id="headingThree8">
																			<div class="card-title collapsed"
																				data-toggle="collapse"
																				data-target="#collapseThree8"
																				aria-expanded="false"
																				aria-controls="collapseThree8">
																				<i
																					class="flaticon-safe-shield-protection"></i>
																				Jabatan Pekerja Enam
																			</div>
																		</div>
																		<div id="collapseThree8" class="collapse"
																			aria-labelledby="headingThree8"
																			data-parent="#accordionExample7">
																			<div class="card-body">
																				Anim pariatur cliche reprehenderit, enim
																				eiusmod high life accusamus terry
																				richardson ad
																				squid. 3 wolf
																				moon officia aute, non cupidatat
																				skateboard dolor brunch. Food truck
																				quinoa nesciunt
																				laborum eiusmod.
																				Brunch 3 wolf moon tempor, sunt aliqua
																				put a bird on it squid single-origin
																				coffee nulla
																				assumenda
																				shoreditch et. Nihil anim keffiyeh
																				helvetica, craft beer labore wes
																				anderson cred
																				nesciunt sapiente ea
																				proident. Ad vegan excepteur butcher
																				vice lomo. Leggings occaecat craft beer
																				farm-to-table, raw denim
																				aesthetic synth nesciunt you probably
																				haven't heard of them accusamus labore
																				sustainable
																				VHS.
																			</div>
																		</div>
																	</div>
																</div>
																<!--end::Accordion-->
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>


					<!-- end:: Section -->

					<!-- end:: Content -->
				</div>
				<!-- end:: Content -->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
</asp:Content>
