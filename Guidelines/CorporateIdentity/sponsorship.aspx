﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CORSEC.Master" AutoEventWireup="true" CodeBehind="sponsorship.aspx.cs" Inherits="Pertamina.CORSEC._2019.Guidelines.CorporateIdentity.sponsorship" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">

    <!-- end:: Header -->
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content_header">

        <!-- begin:: Content Head -->
        <div class="kt-subheader  kt-grid__item" id="kt_subheader">
            <div class="kt-container  kt-container--fluid ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">Dashboard</h3>
                    <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                    <a href="#" class="btn btn-label-success btn-bold btn-sm btn-icon-h kt-margin-l-10">
                        <i class="fa fa-plus"></i>Tambah Data
                    </a>
                    <div class="kt-input-icon kt-input-icon--right kt-subheader__search kt-hidden">
                        <input type="text" class="form-control" placeholder="Search order..." id="generalSearch">
                        <span class="kt-input-icon__icon kt-input-icon__icon--right">
                            <span><i class="flaticon2-search-1"></i></span>
                        </span>
                    </div>
                </div>
                <div class="kt-subheader__toolbar">
                    <div class="kt-subheader__wrapper">
                        <a href="#" class="btn kt-subheader__btn-daterange" id="kt_dashboard_daterangepicker" data-toggle="kt-tooltip" title="Select dashboard daterange" data-placement="left">
                            <span class="kt-subheader__btn-daterange-title" id="kt_dashboard_daterangepicker_title">Today</span>&nbsp;
											<span class="kt-subheader__btn-daterange-date" id="kt_dashboard_daterangepicker_date">Aug 16</span>
                            <i class="flaticon2-calendar-1"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- end:: Content Head -->

    <!-- begin:: Content -->
    <div class="kt-content  kt-content--fit-top  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <!-- begin:: Content -->

        <!-- begin:: Hero -->
        <div class="kt-sc" style="background-image: url('<%: ResolveUrl("~/Content/assets/bg/bg-9.jpg") %>')">
            <div class="kt-container ">

                <div class="kt-sc__bottom">
                    <h3 class="kt-sc__heading kt-heading kt-heading--center kt-heading--xxl kt-heading--medium">Sponsorship
                    </h3>

                </div>
            </div>
        </div>
        <!-- end:: Hero -->


        <!-- begin:: Section -->
        <div class="kt-container ">
            <div class="kt-portlet">
                <div class="kt-portlet__body">
                    <div class="kt-infobox">
                        <div class="kt-infobox__header">
                            <h2 class="kt-infobox__title">Sponsorship Pertamina</h2>
                        </div>
                        <div class="kt-infobox__body">
                            <div class="kt-infobox__section">
                                <div class="kt-infobox__content">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus at laboriosam architecto maiores consequuntur pariatur fuga aperiam labore, consectetur ratione ullam, accusamus quos optio quibusdam molestias repellendus! Aut nulla dolores nisi nemo aperiam et quos magni ut officia, nesciunt quia ipsa illo nam quibusdam possimus dignissimos ducimus veritatis quae tempore amet voluptate repellat. Eos sed est numquam nisi hic eveniet quos, rerum culpa ipsa,
													<br />
                                    <br />
                                    sunt odit ducimus unde recusandae harum eligendi nihil doloribus, libero error dolore esse impedit quam cum! Molestias, adipisci, reprehenderit. Quasi sequi corporis explicabo perferendis? Minus voluptatum corporis earum saepe, ipsa quo nulla deserunt, sed suscipit sapiente eius facilis nisi necessitatibus. Obcaecati nisi natus, laboriosam quo quibusdam nesciunt numquam blanditiis. Recusandae tenetur odio accusantium quaerat, facere est, atque magni laboriosam repellat, cupiditate voluptatum eligendi eum suscipit doloremque laborum cumque consequuntur optio veniam nobis non ducimus! Voluptatibus laborum numquam fuga laboriosam distinctio explicabo reprehenderit minima saepe dicta tempora!
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>


        <div class="kt-container">
            <div class="kt-portlet">
                <div class="kt-portlet__body">
                    <div class="kt-infobox">
                        <div class="kt-infobox__body">
                            <div class="kt-infobox__section">
                                <div class="kt-infobox__content">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="kt-widget19__pic kt-portlet-fit--top kt-portlet-fit--sides"
                                                style="min-height: 200px; background-image: url(<%: ResolveUrl("~/Content/assets/media/products/product1.jpg") %>)">
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus at laboriosam
																	architecto maiores consequuntur pariatur fuga aperiam labore, consectetur ratione ullam,
																	accusamus quos optio quibusdam molestias repellendus! Aut nulla dolores nisi nemo aperiam et
																	quos magni ut officia, nesciunt quia ipsa illo nam quibusdam possimus dignissimos ducimus
																	veritatis quae tempore amet voluptate repellat. Eos sed est numquam nisi hic eveniet quos,
																	rerum culpa ipsa, Obcaecati nisi natus, laboriosam quo quibusdam
																	nesciunt numquam blanditiis.
                                            </p>
                                        </div>
                                    </div>
                                    sunt odit ducimus unde recusandae harum eligendi nihil doloribus, libero error dolore esse
														impedit quam cum! Molestias, adipisci, reprehenderit. Quasi sequi corporis explicabo
														perferendis? Minus voluptatum corporis earum saepe, ipsa quo nulla deserunt, sed suscipit
														sapiente eius facilis nisi necessitatibus. Obcaecati nisi natus, laboriosam quo quibusdam
														nesciunt numquam blanditiis. Recusandae tenetur odio accusantium quaerat, facere est, atque
														magni laboriosam repellat, cupiditate voluptatum eligendi eum suscipit doloremque laborum
														cumque consequuntur optio veniam nobis non ducimus! Voluptatibus laborum numquam fuga
														laboriosam distinctio explicabo reprehenderit minima saepe dicta tempora!
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- end:: Section -->

        <!-- begin:: Section -->
        <div class="kt-container ">
            <div class="row">
                <div class="col-lg-6">
                    <div class="kt-portlet kt-callout">
                        <div class="kt-portlet__body">
                            <div class="kt-callout__body">
                                <div class="kt-callout__content">
                                    <h3 class="kt-callout__title">Lorem ipsum dolor sit amet consectetur adipisicing</h3>
                                    <p class="kt-callout__desc">
                                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Earum quod fugit incidunt voluptatem corporis obcaecati omnis autem illo ipsum nam! Est nesciunt, quo voluptatum distinctio sint nostrum corporis fugiat tempora suscipit, deleniti harum dolorem, aliquam dolorum facilis voluptas error delectus rem ut earum officia fugit quidem eius. Inventore, et explicabo.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="kt-portlet kt-callout">
                        <div class="kt-portlet__body">
                            <div class="kt-callout__body">
                                <div class="kt-callout__content">
                                    <h3 class="kt-callout__title">Porro esse provident molestiae? quasi ducimus!</h3>
                                    <p class="kt-callout__desc">
                                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Explicabo aliquid doloribus consequuntur ullam cupiditate repellendus autem minus facilis libero delectus at, sunt veniam consectetur obcaecati eum? Necessitatibus dolorum aliquam itaque perspiciatis voluptates commodi est quidem praesentium. Distinctio nisi odit, voluptatem architecto corporis saepe magni, alias eaque perferendis sed animi expedita!
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- end:: Section -->

        <!-- end:: Content -->
    </div>
    <!-- end:: Content -->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
</asp:Content>
