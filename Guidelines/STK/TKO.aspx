﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CORSEC.Master" AutoEventWireup="true" CodeBehind="TKO.aspx.cs" Inherits="Pertamina.CORSEC._2019.Guidelines.STK.TKO" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">

    <!-- end:: Header -->
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content_header">

        <!-- begin:: Content Head -->
        <div class="kt-subheader  kt-grid__item" id="kt_subheader">
            <div class="kt-container  kt-container--fluid ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">Dashboard</h3>
                    <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                    <a href="#" class="btn btn-label-success btn-bold btn-sm btn-icon-h kt-margin-l-10">
                        <i class="fa fa-plus"></i>Tambah Data
                    </a>
                    <div class="kt-input-icon kt-input-icon--right kt-subheader__search kt-hidden">
                        <input type="text" class="form-control" placeholder="Search order..." id="generalSearch">
                        <span class="kt-input-icon__icon kt-input-icon__icon--right">
                            <span><i class="flaticon2-search-1"></i></span>
                        </span>
                    </div>
                </div>
                <div class="kt-subheader__toolbar">
                    <div class="kt-subheader__wrapper">
                        <a href="#" class="btn kt-subheader__btn-daterange" id="kt_dashboard_daterangepicker" data-toggle="kt-tooltip" title="Select dashboard daterange" data-placement="left">
                            <span class="kt-subheader__btn-daterange-title" id="kt_dashboard_daterangepicker_title">Today</span>&nbsp;
											<span class="kt-subheader__btn-daterange-date" id="kt_dashboard_daterangepicker_date">Aug 16</span>
                            <i class="flaticon2-calendar-1"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- end:: Content Head -->

    <!-- begin:: Content -->
    <div class="kt-content kt-content--fit-top  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <!-- begin:: Content -->

        <div class="kt-container kt-pt10">
            <div class="kt-portlet">
                <div class="kt-portlet__body">
                    <div class="kt-infobox">
                        <div class="kt-infobox__header">
                            <h2 class="kt-infobox__title">TKO</h2>
                        </div>
                        <div class="kt-infobox__body">
                            <div class="kt-infobox__section">
                                <div class="kt-infobox__content">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus at laboriosam
													architecto maiores consequuntur pariatur fuga aperiam labore, consectetur ratione ullam,
													accusamus quos optio quibusdam molestias repellendus! Aut nulla dolores nisi nemo aperiam et
													quos magni ut officia, nesciunt quia ipsa illo nam quibusdam possimus dignissimos ducimus
													veritatis quae tempore amet voluptate repellat. Eos sed est numquam nisi hic eveniet quos,
													rerum culpa ipsa,
													<br>
                                    <br>
                                    sunt odit ducimus unde recusandae harum eligendi nihil doloribus, libero error dolore esse
													impedit quam cum! Molestias, adipisci, reprehenderit. Quasi sequi corporis explicabo
													perferendis? Minus voluptatum corporis earum saepe, ipsa quo nulla deserunt, sed suscipit
													sapiente eius facilis nisi necessitatibus. Obcaecati nisi natus, laboriosam quo quibusdam
													nesciunt numquam blanditiis. Recusandae tenetur odio accusantium quaerat, facere est, atque
													magni laboriosam repellat, cupiditate voluptatum eligendi eum suscipit doloremque laborum
													cumque consequuntur optio veniam nobis non ducimus! Voluptatibus laborum numquam fuga
													laboriosam distinctio explicabo reprehenderit minima saepe dicta tempora!
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- end:: Section -->

        <!-- end:: Content -->
    </div>
    <!-- end:: Content -->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
</asp:Content>
