﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CORSEC.Master" AutoEventWireup="true" CodeBehind="TKI.aspx.cs" Inherits="Pertamina.CORSEC._2019.Guidelines.STK.TKI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <!-- end:: Header -->
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <!-- begin:: Content Head -->
        <div class="kt-subheader  kt-grid__item" id="kt_subheader">
            <div class="kt-container  kt-container--fluid ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">Dashboard</h3>
                    <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                    <a href="#" class="btn btn-label-success btn-bold btn-sm btn-icon-h kt-margin-l-10">
                        <i class="fa fa-plus"></i>Tambah Data
                    </a>
                    <div class="kt-input-icon kt-input-icon--right kt-subheader__search kt-hidden">
                        <input type="text" class="form-control" placeholder="Search order..." id="generalSearch">
                        <span class="kt-input-icon__icon kt-input-icon__icon--right">
                            <span><i class="flaticon2-search-1"></i></span>
                        </span>
                    </div>
                </div>
                <div class="kt-subheader__toolbar">
                    <div class="kt-subheader__wrapper">
                        <a href="#" class="btn kt-subheader__btn-daterange" id="kt_dashboard_daterangepicker" data-toggle="kt-tooltip" title="Select dashboard daterange" data-placement="left">
                            <span class="kt-subheader__btn-daterange-title" id="kt_dashboard_daterangepicker_title">Today</span>&nbsp;
											<span class="kt-subheader__btn-daterange-date" id="kt_dashboard_daterangepicker_date">Aug 16</span>
                            <i class="flaticon2-calendar-1"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- end:: Content Head -->

    <div class="kt-container kt-pt10 kt-container--fluid  kt-grid__item kt-grid__item--fluid">

        <!--begin:: Portlet-->
        <div class="kt-portlet">
            <div class="kt-portlet__body">
                <div class="kt-widget kt-widget--user-profile-3">
                    <div class="kt-widget__top">
                        <div class="kt-widget__media kt-hidden-">
                            <img src="<%: ResolveUrl("~/Content/assets/media/project-logos/3.png") %>" alt="image">
                        </div>
                        <div class="kt-widget__content">
                            <div class="kt-widget__head">
                                <a href="#" class="kt-widget__username">Dokumen TKI Download
                                </a>
                                <div class="kt-widget__action">
                                    <button type="button" class="btn btn-brand btn-sm btn-upper">Download</button>
                                </div>
                            </div>
                            <div class="kt-widget__info">
                                <div class="kt-widget__desc">
                                    I distinguish three main text objektive could be merely to inform people.
														<br>
                                    A second could be persuade people.You want people to bay objective
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end:: Portlet-->

        <!--begin:: Portlet-->
        <div class="kt-portlet">
            <div class="kt-portlet__body">
                <div class="kt-widget kt-widget--user-profile-3">
                    <div class="kt-widget__top">
                        <div class="kt-widget__media kt-hidden-">
                            <img src="<%: ResolveUrl("~/Content/assets/media/project-logos/3.png") %>" alt="image">
                        </div>
                        <div class="kt-widget__content">
                            <div class="kt-widget__head">
                                <a href="#" class="kt-widget__username">Dokumen TKI Download
                                </a>
                                <div class="kt-widget__action">
                                    <button type="button" class="btn btn-secondary btn-sm btn-upper">Download</button>
                                </div>
                            </div>
                            <div class="kt-widget__info">
                                <div class="kt-widget__desc">
                                    I distinguish three main text objektive could be merely to inform people.
														<br>
                                    A second could be persuade people.You want people to bay objective
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end:: Portlet-->

        <!--begin:: Portlet-->
        <div class="kt-portlet">
            <div class="kt-portlet__body">
                <div class="kt-widget kt-widget--user-profile-3">
                    <div class="kt-widget__top">
                        <div class="kt-widget__media kt-hidden-">
                            <img src="<%: ResolveUrl("~/Content/assets/media/project-logos/3.png") %>" alt="image">
                        </div>
                        <div class="kt-widget__content">
                            <div class="kt-widget__head">
                                <a href="#" class="kt-widget__username">Dokumen TKI Download
                                </a>
                                <div class="kt-widget__action">
                                    <button type="button" class="btn btn-brand btn-sm btn-upper">Download</button>
                                </div>
                            </div>
                            <div class="kt-widget__info">
                                <div class="kt-widget__desc">
                                    I distinguish three main text objektive could be merely to inform people.
														<br>
                                    A second could be persuade people.You want people to bay objective
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end:: Portlet-->

        <!--begin:: Portlet-->
        <div class="kt-portlet">
            <div class="kt-portlet__body">
                <div class="kt-widget kt-widget--user-profile-3">
                    <div class="kt-widget__top">
                        <div class="kt-widget__media kt-hidden-">
                            <img src="<%: ResolveUrl("~/Content/assets/media/project-logos/3.png") %>" alt="image">
                        </div>
                        <div class="kt-widget__content">
                            <div class="kt-widget__head">
                                <a href="#" class="kt-widget__username">Dokumen TKI Download
                                </a>
                                <div class="kt-widget__action">
                                    <button type="button" class="btn btn-brand btn-sm btn-upper">Download</button>
                                </div>
                            </div>
                            <div class="kt-widget__info">
                                <div class="kt-widget__desc">
                                    I distinguish three main text objektive could be merely to inform people.
														<br>
                                    A second could be persuade people.You want people to bay objective
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end:: Portlet-->

        <!--begin:: Portlet-->
        <div class="kt-portlet">
            <div class="kt-portlet__body">
                <div class="kt-widget kt-widget--user-profile-3">
                    <div class="kt-widget__top">
                        <div class="kt-widget__media kt-hidden-">
                            <img src="<%: ResolveUrl("~/Content/assets/media/project-logos/3.png") %>" alt="image">
                        </div>
                        <div class="kt-widget__content">
                            <div class="kt-widget__head">
                                <a href="#" class="kt-widget__username">Dokumen TKI Download
                                </a>
                                <div class="kt-widget__action">
                                    <button type="button" class="btn btn-secondary btn-sm btn-upper">Download</button>
                                </div>
                            </div>
                            <div class="kt-widget__info">
                                <div class="kt-widget__desc">
                                    I distinguish three main text objektive could be merely to inform people.
														<br>
                                    A second could be persuade people.You want people to bay objective
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end:: Portlet-->



        <!--Begin::Pagination-->
        <div class="row">
            <div class="col-xl-12">

                <!--begin:: Components/Pagination/Default-->
                <div class="kt-portlet">
                    <div class="kt-portlet__body">

                        <!--begin: Pagination-->
                        <div class="kt-pagination kt-pagination--brand">
                            <ul class="kt-pagination__links">
                                <li class="kt-pagination__link--first">
                                    <a href="#"><i class="fa fa-angle-double-left kt-font-brand"></i></a>
                                </li>
                                <li class="kt-pagination__link--next">
                                    <a href="#"><i class="fa fa-angle-left kt-font-brand"></i></a>
                                </li>
                                <li>
                                    <a href="#">...</a>
                                </li>
                                <li>
                                    <a href="#">29</a>
                                </li>
                                <li>
                                    <a href="#">30</a>
                                </li>
                                <li class="kt-pagination__link--active">
                                    <a href="#">31</a>
                                </li>
                                <li>
                                    <a href="#">32</a>
                                </li>
                                <li>
                                    <a href="#">33</a>
                                </li>
                                <li>
                                    <a href="#">34</a>
                                </li>
                                <li>
                                    <a href="#">...</a>
                                </li>
                                <li class="kt-pagination__link--prev">
                                    <a href="#"><i class="fa fa-angle-right kt-font-brand"></i></a>
                                </li>
                                <li class="kt-pagination__link--last">
                                    <a href="#"><i class="fa fa-angle-double-right kt-font-brand"></i></a>
                                </li>
                            </ul>
                            <div class="kt-pagination__toolbar">
                                <select class="form-control kt-font-brand" style="width: 60px">
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="30">30</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select>
                                <span class="pagination__desc">Displaying 10 of 230 records
                                </span>
                            </div>
                        </div>

                        <!--end: Pagination-->
                    </div>
                </div>

                <!--end:: Components/Pagination/Default-->
            </div>
        </div>

        <!--End::Pagination-->
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
</asp:Content>
