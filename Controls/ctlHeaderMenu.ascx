﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctlHeaderMenu.ascx.cs" Inherits="Pertamina.CORSEC._2019.Controls.ctlHeaderMenu" %>
<!-- Uncomment this to display the close button of the panel
<button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
-->
<div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">
    <div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile  kt-header-menu--layout-default ">
        <ul class="kt-menu__nav ">
           <%-- <li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--rel kt-menu__item--active" data-ktmenu-submenu-toggle="click" aria-haspopup="true">
                <a href="javascript:;" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-text">CORRESPONDENCE</span></a>
                <div class="kt-menu__submenu kt-menu__submenu--classic kt-menu__submenu--left">
                    <ul class="kt-menu__subnav">
                        <li class="kt-menu__item " aria-haspopup="true"><a href="index.html" class="kt-menu__link"><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Strategi Komunikasi Korporat</span></a></li>
                        <li class="kt-menu__item " aria-haspopup="true"><a href="index.html" class="kt-menu__link"><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Brand Development Strategy</span></a></li>
                        <li class="kt-menu__item " aria-haspopup="true"><a href="index.html" class="kt-menu__link"><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Strategi Pengelolaan Krisis Komunikasi</span></a></li>
                    </ul>
                </div>
            </li>--%>
            <li class="kt-menu__item kt-menu__item--submenu kt-menu__item--rel" data-ktmenu-submenu-toggle="click" aria-haspopup="true">
                <a href="http://intra.pertamina.com/ecorrespondence" target="_blank" class="kt-menu__link" ><span class="kt-menu__link-text">CORRESPONDENCE</span></a>
               <%-- <div class="kt-menu__submenu kt-menu__submenu--classic">
                    <ul class="kt-menu__subnav">
                        <li class="kt-menu__item " aria-haspopup="true"><a href="index.html" class="kt-menu__link"><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Stakeholders Engagement</span></a></li>
                    </ul>
                </div>--%>
            </li>
            <li class="kt-menu__item kt-menu__item--submenu kt-menu__item--rel" data-ktmenu-submenu-toggle="click" aria-haspopup="true">
                <a href="http://intra-iam.pertamina.com/Account/Login" target="_blank"  class="kt-menu__link"><span class="kt-menu__link-text">I&nbsp;-&nbsp;AM</span></a>
                <%--<div class="kt-menu__submenu kt-menu__submenu--classic">
                    <ul class="kt-menu__subnav">
                        <li class="kt-menu__item " aria-haspopup="true"><a href="index.html" class="kt-menu__link"><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Realisasi CSR</span></a></li>
                        <li class="kt-menu__item " aria-haspopup="true"><a href="index.html" class="kt-menu__link"><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Realisasi BL</span></a></li>
                        <li class="kt-menu__item " aria-haspopup="true"><a href="index.html" class="kt-menu__link"><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Realisasi PK</span></a></li>
                        <li class="kt-menu__item " aria-haspopup="true"><a href="index.html" class="kt-menu__link"><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Kolektabilitas PK</span></a></li>
                        <li class="kt-menu__item " aria-haspopup="true"><a href="index.html" class="kt-menu__link"><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">OPEX PKBL</span></a></li>
                    </ul>
                </div>--%>
            </li>
            <%--<li class="kt-menu__item kt-menu__item--submenu kt-menu__item--rel" data-ktmenu-submenu-toggle="click" aria-haspopup="true">
                <a href="javascript:;" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-text">Design Grafis</span></a>
                <div class="kt-menu__submenu kt-menu__submenu--classic">
                    <ul class="kt-menu__subnav">
                        <li class="kt-menu__item " aria-haspopup="true"><a href="index.html" class="kt-menu__link"><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Desain</span></a></li>
                        <li class="kt-menu__item " aria-haspopup="true"><a href="index.html" class="kt-menu__link"><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Infografis</span></a></li>
                    </ul>
                </div>
            </li>
            <li class="kt-menu__item kt-menu__item--submenu kt-menu__item--rel" data-ktmenu-submenu-toggle="click" aria-haspopup="true">
                <a href="javascript:;" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-text">Monitoring</span></a>
                <div class="kt-menu__submenu kt-menu__submenu--classic">
                    <ul class="kt-menu__subnav">
                        <li class="kt-menu__item " aria-haspopup="true"><a href="index.html" class="kt-menu__link"><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Media Monitoring</span></a></li>
                        <li class="kt-menu__item " aria-haspopup="true"><a href="index.html" class="kt-menu__link"><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Data & Information</span></a></li>
                        <li class="kt-menu__item " aria-haspopup="true"><a href="index.html" class="kt-menu__link"><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Risk Management</span></a></li>
                        <li class="kt-menu__item " aria-haspopup="true"><a href="index.html" class="kt-menu__link"><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Corporate Integrated Survey</span></a></li>
                    </ul>
                </div>
            </li>--%>
        </ul>
    </div>
</div>

<!-- end:: Header Menu -->
