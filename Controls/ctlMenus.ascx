﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ctlMenus.ascx.cs" Inherits="Pertamina.CORSEC._2019.Controls.ctlMenus" %>
<!-- begin:: Aside Menu -->
<div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
    <div id="kt_aside_menu" class="kt-aside-menu mg-0" data-ktmenu-vertical="1" data-ktmenu-scroll="1" data-ktmenu-dropdown-timeout="500">
        <ul class="kt-menu__nav pd-0" id="mainNav" runat="server"></ul>
        
<%--        <ul class="kt-menu__nav pd-0">
            <li class="kt-menu__item kt-menu__item--open kt-menu__item--here">
                <a href="index.html" class="kt-menu__link">
                    <span class="kt-menu__link-icon"><i class="fa fa-home"></i></span>
                    <span class="kt-menu__link-text">Beranda</span>
                </a>
            </li>
            <li class="kt-menu__item kt-menu__item--submenu">
                <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                    <span class="kt-menu__link-icon"><i class="fa fa-file-invoice"></i></span>
                    <span class="kt-menu__link-text">About</span><i class="kt-menu__ver-arrow la la-angle-right"></i>
                </a>
                <div class="kt-menu__submenu ">
                    <span class="kt-menu__arrow"></span>
                    <ul class="kt-menu__subnav">
                        <li class="kt-menu__item kt-menu__item--submenu">
                            <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                                    <span></span></i>
                                <span class="kt-menu__link-text">Profil Corsec</span>
                                <i class="kt-menu__ver-arrow la la-angle-right"></i>
                            </a>
                            <div class="kt-menu__submenu ">
                                <span class="kt-menu__arrow"></span>
                                <ul class="kt-menu__subnav">
                                    <li class="kt-menu__item "><a href="visi-misi.html" class="kt-menu__link"><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Overview, Visi & Misi</span></a></li>
                                    <li class="kt-menu__item "><a href="strategic-partner.html" class="kt-menu__link"><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Strategic Partner</span></a></li>
                                </ul>
                            </div>
                        </li>
                        <li class="kt-menu__item kt-menu__item--submenu">
                            <a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Organisasi Sekper</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
                            <div class="kt-menu__submenu ">
                                <span class="kt-menu__arrow"></span>
                                <ul class="kt-menu__subnav">
                                    <li class="kt-menu__item "><a href="struktur.html" class="kt-menu__link"><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Struktur</span></a></li>
                                    <li class="kt-menu__item "><a href="pekerja.html" class="kt-menu__link"><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Pekerja /  Kontrak</span></a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </li>

            <li class="kt-menu__item kt-menu__item--submenu">
                <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                    <span class="kt-menu__link-icon"><i class="fa fa-file-signature"></i></span>
                    <span class="kt-menu__link-text">Guidelines & Policy</span><i class="kt-menu__ver-arrow la la-angle-right"></i>
                </a>
                <div class="kt-menu__submenu ">
                    <span class="kt-menu__arrow"></span>
                    <ul class="kt-menu__subnav">
                        <li class="kt-menu__item kt-menu__item--submenu">
                            <a href="stk.html" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">STK</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
                            <div class="kt-menu__submenu ">
                                <span class="kt-menu__arrow"></span>
                                <ul class="kt-menu__subnav">
                                    <li class="kt-menu__item "><a href="pedoman.html" class="kt-menu__link"><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Pedoman</span></a></li>
                                    <li class="kt-menu__item "><a href="tko.html" class="kt-menu__link"><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">TKO</span></a></li>
                                    <li class="kt-menu__item "><a href="tki.html" class="kt-menu__link"><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">TKI</span></a></li>
                                </ul>
                            </div>
                        </li>
                        <li class="kt-menu__item kt-menu__item--submenu">
                            <a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Corporate Identity</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
                            <div class="kt-menu__submenu ">
                                <span class="kt-menu__arrow"></span>
                                <ul class="kt-menu__subnav">
                                    <li class="kt-menu__item "><a href="javascript:;" class="kt-menu__link"><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Merchandise Hub</span></a></li>
                                    <li class="kt-menu__item "><a href="javascript:;" class="kt-menu__link"><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Corporate Template</span></a></li>
                                    <li class="kt-menu__item "><a href="javascript:;" class="kt-menu__link"><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Sponsorship</span></a></li>
                                </ul>
                            </div>
                        </li>
                        <li class="kt-menu__item "><a href="javascript:;" class="kt-menu__link"><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Kebijakan Pemberian Informasis</span></a></li>
                    </ul>
                </div>
            </li>

            <li class="kt-menu__item kt-menu__item--submenu">
                <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                    <span class="kt-menu__link-icon"><i class="fa fa-archive"></i></span>
                    <span class="kt-menu__link-text">Program</span><i class="kt-menu__ver-arrow la la-angle-right"></i>
                </a>
                <div class="kt-menu__submenu ">
                    <span class="kt-menu__arrow"></span>
                    <ul class="kt-menu__subnav">
                        <li class="kt-menu__item "><a href="javascript:;" class="kt-menu__link"><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Corporate Communication</span></a></li>
                        <li class="kt-menu__item "><a href="javascript:;" class="kt-menu__link"><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Stakeholders Relation</span></a></li>
                        <li class="kt-menu__item "><a href="javascript:;" class="kt-menu__link"><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">CSR SMEPP</span></a></li>
                        <li class="kt-menu__item "><a href="javascript:;" class="kt-menu__link"><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">BOD Support</span></a></li>
                        <li class="kt-menu__item "><a href="javascript:;" class="kt-menu__link"><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Planning & Governance</span></a></li>
                    </ul>
                </div>
            </li>

            <li class="kt-menu__item kt-menu__item--submenu">
                <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                    <span class="kt-menu__link-icon"><i class="fa fa-calendar-day"></i></span>
                    <span class="kt-menu__link-text">Event & Information</span><i class="kt-menu__ver-arrow la la-angle-right"></i>
                </a>
                <div class="kt-menu__submenu ">
                    <span class="kt-menu__arrow"></span>
                    <ul class="kt-menu__subnav">
                        <li class="kt-menu__item ">
                            <a href="trends-issues.html" class="kt-menu__link">
                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Trends & Issues</span>
                            </a>
                        </li>
                        <li class="kt-menu__item ">
                            <a href="kalender-korporat.html" class="kt-menu__link">
                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Kalender Event Korpoat</span>
                            </a>
                        </li>
                        <li class="kt-menu__item ">
                            <a href="volunteer.html" class="kt-menu__link">
                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Volunteer</span>
                            </a>
                        </li>
                        <li class="kt-menu__item ">
                            <a href="informasi-magang.html" class="kt-menu__link">
                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Informasi Magang
                                </span>
                            </a>
                        </li>
                        <li class="kt-menu__item ">
                            <a href="gallery.html" class="kt-menu__link">
                                <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Galeri
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>

            <li class="kt-menu__item kt-menu__item--submenu">
                <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                    <span class="kt-menu__link-icon"><i class="fa fa-archive"></i></span>
                    <span class="kt-menu__link-text">Speech & Report</span><i class="kt-menu__ver-arrow la la-angle-right"></i>
                </a>
                <div class="kt-menu__submenu ">
                    <span class="kt-menu__arrow"></span>
                    <ul class="kt-menu__subnav">
                        <li class="kt-menu__item "><a href="broadspeech.html" class="kt-menu__link"><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Board Speech</span></a></li>
                        <li class="kt-menu__item "><a href="presentasi.html" class="kt-menu__link"><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Presentasi</span></a></li>
                        <li class="kt-menu__item "><a href="pertapedia.html" class="kt-menu__link"><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Pertapedia</span> </a></li>
                        <li class="kt-menu__item "><a href="javascript:;" class="kt-menu__link"><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Laporan Kerja Sekper</span> </a></li>
                        <li class="kt-menu__item "><a href="pojok-kreasi.html" class="kt-menu__link"><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Pojok Kreasi</span></a></li>
                    </ul>
                </div>
            </li>

        </ul>--%>

    </div>
</div>

<!-- end:: Aside Menu -->
