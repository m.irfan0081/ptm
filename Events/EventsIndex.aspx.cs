﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Pertamina.CORSEC._2019.Events
{
    public partial class EventsIndex : System.Web.UI.Page
    {
        public List<GetRequestOrder.New_GetList_OperationResponse> Resp_ = new List<GetRequestOrder.New_GetList_OperationResponse>();
        public List<GetRequestOrder.New_GetList_OperationResponse> Resp_Sub = new List<GetRequestOrder.New_GetList_OperationResponse>();
        public int counter;
        public int counterSub;
        protected void Page_PreInit(object sender, EventArgs e)
        {
            var dateString2 = DateTime.Now.ToString("yyyy-MM-dd");
            GetRequestOrder.AuthenticationInfo User = new GetRequestOrder.AuthenticationInfo();
            User.userName = "corpsecintegration";
            User.password = "Pertamin@123";
            this.counter = 0;
            this.counterSub = 0;
            List<GetRequestOrder.New_GetList_OperationResponse> _RESP = new List<GetRequestOrder.New_GetList_OperationResponse>();
            GetRequestOrder.New_Port_0PortTypeClient eventService = new GetRequestOrder.New_Port_0PortTypeClient();
            eventService.Open();
            for (int i = 0; i < 3; i++)
            {
                GetRequestOrder.New_GetList_OperationRequest req = new GetRequestOrder.New_GetList_OperationRequest(User, "'Summary' = \"Request Feature Input Event Dashboard\"", "" + i + "", "1");
                try
                {
                    var resp = eventService.New_GetList_OperationAsync(req);
                    this.Resp_.Add(resp.Result);
                }
                catch (Exception execption)
                {
                    Console.WriteLine(execption);
                }

            }
            for (int i = 2; i < 5; i++)
            {
                GetRequestOrder.New_GetList_OperationRequest req = new GetRequestOrder.New_GetList_OperationRequest(User, "'Summary' = \"Request Feature Input Event Dashboard\"", "" + i + "", "1");
                try
                {
                    var resp = eventService.New_GetList_OperationAsync(req);
                    this.Resp_Sub.Add(resp.Result);
                }
                catch (Exception execption)
                {
                    Console.WriteLine(execption);
                }
            }


            eventService.Close();
            this.Resp_Sub.ForEach(_item =>
            {
                _item.NamaEvent = shorten(_item.NamaEvent, 35);
            });
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected string shorten(string contentString, int num)
        {
            if (contentString.Length <= num)
            {
                return contentString;
          }
            else
            {
                return contentString.Substring(0, num) + "....";
          }
        }
    }
}