﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Pertamina.CORSEC._2019.Events
{
    public partial class EventsList : System.Web.UI.Page
    {
        public List<GetRequestOrder.New_GetList_OperationResponse> Resp_ = new List<GetRequestOrder.New_GetList_OperationResponse>();
        public List<GetRequestOrder.New_GetList_OperationResponse> Resp_Banner = new List<GetRequestOrder.New_GetList_OperationResponse>();
        public List<GetWorkOrder.New_GetList_Operation_0Response> Resp_WO = new List<GetWorkOrder.New_GetList_Operation_0Response>();
        public string Test;
        public Boolean onLoad;
        protected void Page_Load(object sender, EventArgs e)
        {
            this.onLoad = true;
            this.Resp_ = new List<GetRequestOrder.New_GetList_OperationResponse>();
            var dateString2 = DateTime.Now.ToString("yyyy-MM-dd");
            GetRequestOrder.AuthenticationInfo User = new GetRequestOrder.AuthenticationInfo();
            User.userName = "corpsecintegration";
            User.password = "Pertamin@123";
            GetRequestOrder.New_Port_0PortTypeClient eventService = new GetRequestOrder.New_Port_0PortTypeClient();

            GetWorkOrder.AuthenticationInfo UserWo = new GetWorkOrder.AuthenticationInfo();
            UserWo.userName = "corpsecintegration";
            UserWo.password = "Pertamin@123";
            GetWorkOrder.New_Port_0PortTypeClient GetWO = new GetWorkOrder.New_Port_0PortTypeClient();
            GetWO.Open();
            try
            {
                var respWO = GetWO.New_GetList_Operation_0Async(UserWo, "'Summary' = \"Request Feature Input Event Dashboard\"", "", "");
                var textTemp = "" + respWO.Result;
                Label1.Text = textTemp;
            }
            catch (Exception execption)
            {
                Console.WriteLine(execption);
            }
            GetWO.Close();
            eventService.Open();
            for (int i = 0; i < 50; i++)
            {
                GetRequestOrder.New_GetList_OperationRequest req = new GetRequestOrder.New_GetList_OperationRequest(User, "'Summary' = \"Request Feature Input Event Dashboard\"", "" + i + "", "1");
                
                try
                {
                    var resp = eventService.New_GetList_OperationAsync(req);
                    if (i < 3)
                    {
                        this.Resp_Banner.Add(resp.Result);
                    }
                    this.Resp_.Add(resp.Result);
                }
                catch (Exception execption)
                {
                    Console.WriteLine(execption);
                }
                finally
                {
                    this.onLoad = false;
                }
            }
            eventService.Close();
            this.Resp_.ForEach(_item =>
            {
                _item.NamaEvent = shorten(_item.NamaEvent, 35);
            });
        }

        protected string shorten(string contentString, int num)
        {
            if (contentString.Length <= num)
            {
                return contentString;
            }
            else
            {
                return contentString.Substring(0, num) + "....";
            }
        }

        protected void Check_Clicked(object sender, EventArgs e)
        {
            this.Resp_ = new List<GetRequestOrder.New_GetList_OperationResponse>();
            this.onLoad = true;
            var dateString2 = DateTime.Now.ToString("yyyy-MM-dd");
            GetRequestOrder.AuthenticationInfo User = new GetRequestOrder.AuthenticationInfo();
            GetRequestOrder.New_Port_0PortTypeClient eventService = new GetRequestOrder.New_Port_0PortTypeClient();
            User.userName = "corpsecintegration";
            User.password = "Pertamin@123";
            string query = "'Summary' = \"Request Feature Input Event Dashboard\"";
            if (checkboxTalkShow.Checked)
            {
                query += " AND 'SR Type Field 33' LIKE \"Talkshow%\"";
            }

            if (checkboxSeremoni.Checked)
            {
                query += " AND 'SR Type Field 33' LIKE \"Seremoni%\"";
            }

            if (checkboxPeresmian.Checked)
            {
                query += " AND 'SR Type Field 33' LIKE \"Peresmian%\"";
            }
            if (checkboxPameran.Checked)
            {
                query += " AND 'SR Type Field 33' LIKE \"Pameran%\"";
            }

            if (checkboxMWTVirtual.Checked)
            {
                query += " AND 'SR Type Field 33' LIKE \"MWT Virtual%\"";
            }

            if (checkboxEventTahunan.Checked)
            {
                query += " AND 'SR Type Field 33' LIKE \"Event Tahunan%\"";
            }

            if (checkboxBranchmarking.Checked)
            {
                query += " AND 'SR Type Field 33' LIKE \"Branchmarking%\"";
            }
            eventService.Open();
            for (int i = 0; i < 12; i++)
            {
                GetRequestOrder.New_GetList_OperationRequest req = new GetRequestOrder.New_GetList_OperationRequest(User, query, "" + i + "", "1");
                try
                {
                    var resp = eventService.New_GetList_OperationAsync(req);
                    this.Resp_.Add(resp.Result);
                }
                catch (Exception execption)
                {
                    Console.WriteLine(execption);
                }
                finally
                {
                    this.onLoad = false;
                }
            }
            eventService.Close();
        }
    }
}
