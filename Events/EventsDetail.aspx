﻿<%@ Page Title="Pertamina Intranet - Event Detail" Language="C#" MasterPageFile="~/CORSEC.Master" AutoEventWireup="true" CodeBehind="EventsDetail.aspx.cs" Inherits="Pertamina.CORSEC._2019.Events.EventsDetail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">

    <!-- end:: Header -->
	<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

		<!-- begin:: Content Head -->
		<div class="kt-subheader  kt-grid__item" id="kt_subheader">
			<div class="kt-container  kt-container--fluid ">
				<div class="kt-subheader__main">
					<h3 class="kt-subheader__title">Detail Event</h3>
					<span class="kt-subheader__separator kt-subheader__separator--v"></span>
					
					<div class="kt-input-icon kt-input-icon--right kt-subheader__search kt-hidden">
						<input type="text" class="form-control" placeholder="Search order..."
							id="generalSearch">
						<span class="kt-input-icon__icon kt-input-icon__icon--right">
							<span><i class="flaticon2-search-1"></i></span>
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- end:: Content Head -->

    <div style="margin-top: -6%;">
        
        <div align="center" style="width: 100%; margin-bottom: 20px">
            <object data="<%= Resp_.LinkBanner %>" type="image/png" style="width: 100%; max-height: 550px; ">
                <img src="/Media/images/img-broken.png" style="width: 100%; " alt="poster event" />  
            </object>
        </div>
        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

            <div class="row">
                <aside class="col-md-4">
                    <div class="card mb-3" style="padding: 10px;">
                        <div class="card-header transparent b-b">
                            <h3 class="f-700 mb-15 mt-md-40 fs-32">Informasi</h3>
                        </div>
                        
                        <ul class="tour-info">
                            <li>
                                <span class="icon"><i class="fa fa-archive"></i></span>
                                <span class="sub-head">Kategori</span>
                                <p><%= Resp_.KategoriEvent %> </p>
                            </li>
                            <li>
                                <span class="icon"><i class="fas fa-calendar"></i></span>
                                <span class="sub-head">Date</span>
                                <p><%= countingDown %> </p>
                            </li>
                            <li>
                                <span class="icon"><i class="fas fa-clock"></i></span>
                                <span class="sub-head">Time</span>
                                <p><%= timeEvent %> </p>
                            </li>
                            <li>
                                <span class="icon"><i class="fas fa-map-marker-alt"></i></span>
                                <span class="sub-head">Location</span>
                                <p><%= Resp_.LokasiEvent %></p>
                            </li>
                            <li>
                                <span class="icon"><i class="fas fa-user"></i></span>
                                <span class="sub-head">Posted By</span>
                                <p><%= Resp_.NamaRequester %> </p>
                            </li>
                        </ul>
                    </div>        
                </aside>
                <div class="col-md-8">
                    <div class="card mb-3">
                        <div class="card-header transparent b-b">
                            <strong style="font-size: 24px"><%= Resp_.NamaEvent %></strong>
                        </div>
                        <div class="card-body has-items-overlay playlist p-5">
                            <p>
                                <%= Resp_.DeskripsiEvent %>
                            </p><br />
                            <a href="<%= Resp_.LinkEvent %>"" target="_BLANK" class="btn btn-outline-primary btn-sm">Register Now</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="card mb-3">
                        <iframe src="http://maps.google.com/maps?q=<%= Resp_.LokasiEvent %>&amp;t=&amp;z=13&amp;ie=UTF8&amp;iwloc=&amp;output=embed"  height="450" frameborder="0" style="border:1;" ></iframe>
                    </div>
                </div>
            </div>
            


        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
</asp:Content>
