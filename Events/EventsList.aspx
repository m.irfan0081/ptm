﻿<%@ Page Title="Pertamina Intranet - List Events" Language="C#" MasterPageFile="~/CORSEC.Master" AutoEventWireup="true" CodeBehind="EventsList.aspx.cs" Inherits="Pertamina.CORSEC._2019.Events.EventsList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <!-- end:: Header -->
	<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

		<!-- begin:: Content Head -->
		<div class="kt-subheader  kt-grid__item" id="kt_subheader">
			<div class="kt-container  kt-container--fluid ">
				<div class="kt-subheader__main">
					<h3 class="kt-subheader__title">More Upcoming Events</h3>
					<span class="kt-subheader__separator kt-subheader__separator--v"></span>
					
					<div class="kt-input-icon kt-input-icon--right kt-subheader__search kt-hidden">
						<input type="text" class="form-control" placeholder="Search order..."
							id="generalSearch">
						<span class="kt-input-icon__icon kt-input-icon__icon--right">
							<span><i class="flaticon2-search-1"></i></span>
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- end:: Content Head -->

    <section class="slider-1 relative" >
        <div class="owl-carousel owl-theme main-slider" >
            <% Resp_Banner.ForEach(item =>
                {%>
            <div class="item" style="height: 400px">
                <div class="each-slider flex-center" style="background-image: url('<%= item.LinkBanner%>');" data-overlay="3">
                    <div class="container-fluid outer-slider">
                        <div class="row">
                            <div class="col-lg-7 text-left">
                                <div class="banner-text-left z-5" data-wow-delay=".6s" style="color: black; position:relative; top: 1%;">
                                                
                                    <div class="sub-banner-heads" style="margin-top: -30%;">
                                        <h3 class="f-700 mb-10 fs-24 merry"><%= item.NamaEvent %></h3>
                                    </div>
                                    <a href="/Event/<%= item.RequestNumber %>" class="btn-rnd btn btn-blue">DETAIL <span><i class="fas fa-chevron-right"></i></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <% }); %>
        </div>
    </section>

    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid" style="margin-top: 20px; margin-bottom: 20px;">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label >Filter By Category</label><br />
                            <div class="form-check form-check-inline">
                                <asp:CheckBox id="checkboxTalkShow" runat="server" class="form-check-input"
                                    AutoPostBack="True"
                                    Text="Talkshow"
                                    OnCheckedChanged="Check_Clicked"/>
                            </div>
                            <div class="form-check form-check-inline">
                               <asp:CheckBox id="checkboxPeresmian" runat="server" class="form-check-input"
                                    AutoPostBack="True"
                                    Text="Peresmian"
                                    OnCheckedChanged="Check_Clicked"/>
                            </div>
                            <div class="form-check form-check-inline">
                               <asp:CheckBox id="checkboxSeremoni" runat="server" class="form-check-input"
                                    AutoPostBack="True"
                                    Text="Seremoni"
                                    OnCheckedChanged="Check_Clicked"/>
                            </div>
                            <div class="form-check form-check-inline">
                                <asp:CheckBox id="checkboxEventTahunan" runat="server" class="form-check-input"
                                    AutoPostBack="True"
                                    Text="Event Tahunan"
                                    OnCheckedChanged="Check_Clicked"/>
                            </div>
                            <div class="form-check form-check-inline">
                                <asp:CheckBox id="checkboxPameran" runat="server" class="form-check-input"
                                    AutoPostBack="True"
                                    Text="Pameran"
                                    OnCheckedChanged="Check_Clicked"/>
                            </div>
                            <div class="form-check form-check-inline">
                                <asp:CheckBox id="checkboxBranchmarking" runat="server" class="form-check-input"
                                    AutoPostBack="True"
                                    Text="Branchmarking"
                                    OnCheckedChanged="Check_Clicked"/>
                            </div>
                            <div class="form-check form-check-inline">
                                <asp:CheckBox id="checkboxMWTVirtual" runat="server" class="form-check-input"
                                    AutoPostBack="True"
                                    Text="MWT Virtual"
                                    OnCheckedChanged="Check_Clicked"/>
                            </div>
                        </div>
                    </div>
                </div>

                <% if (Resp_.Count > 0 && !onLoad)
                    { %>
                        <div class="row" >
                        <% Resp_.ForEach(item =>
                            { %>
                            <div class="col-md-4">
                                <a href="/Event/<%= item.RequestNumber %>" style="margin: 5px;">
                                    <div class="card" style="width: 100%; padding: 10px;">
                                        <object data="<%= item.LinkBanner %>" type="image/png" style="width: 100%; height: 200px">
                                            <img src="/Media/images/img-broken.png" class="card-img-top" style="width: 100%; " alt="Event Poster" />  
                                        </object>
                                        <div class="card-body" style="text-align: center; background-color: #f5e8e6; height: 100px">
                                            <h5 class="card-title" style="color: black;"><b><%= item.NamaEvent %></b></h5>
                                            <p class="card-text">
                                                <span><%= item.TanggalPelaksanaanDari %></span>
                                            </p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <% }); %>
                        </div>
                 <% }
                    else
                    {%>
                        <div class="container card" style="margin-top: 5%;">
                            <div class="col-xl-8 mx-lg-auto">
                                <div class="pt-5  text-center">
                                    <i class="icon icon-calendar-times-o" style="color: red; font-size: 80pt; margin-bottom: 5%;"></i>
                                    <h1 class="text-primary">oops!</h1>
                                    <p class="section-subtitle">No Upcoming Event <br />Please Come Back Later!</p>
                                    <asp:Label id="Label1" 
                                     runat="server"/>
                                </div>
                            </div>
                        </div>
                <%  } %>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
</asp:Content>
