﻿<%@ Page Title="Pertamina Intranet - Events" Language="C#" MasterPageFile="~/CORSEC.Master" AutoEventWireup="true" CodeBehind="EventsIndex.aspx.cs" Inherits="Pertamina.CORSEC._2019.Events.EventsIndex" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <!-- end:: Header -->
	<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
		<!-- begin:: Content Head -->
		<div class="kt-subheader  kt-grid__item" id="kt_subheader">
			<div class="kt-container  kt-container--fluid ">
				<div class="kt-subheader__main">
					<h3 class="kt-subheader__title">Upcoming Events</h3>
					<span class="kt-subheader__separator kt-subheader__separator--v"></span>
					
				</div>
			</div>
		</div>
	</div>
    
	<!-- end:: Content Head -->
    <div >
        <% if (Resp_.Count == 0 && Resp_Sub.Count == 0)
            {  %>
            <div class="container card" >
                <div class="col-xl-8 mx-lg-auto">
                    <div class="pt-5  text-center">
                        <h1 class="text-primary">oops!</h1>
                        <p class="section-subtitle">Maaf saat ini event belum tersedia.</p>
                    </div>
                </div>
            </div>
        <% } %>
        <% if (Resp_.Count > 0 )
            {  %>
            <div class="kt-sc" >
                <section class="slider-1 relative" >
                    <div class="owl-carousel owl-theme main-slider" >
                        <% Resp_.ForEach(item =>
                            {%>
                        <div class="item" style="height: 400px">
                            <div class="each-slider flex-center" style="background-image: url('<%= item.LinkBanner%>');" data-overlay="3">
                                <div class="container-fluid outer-slider">
                                    <div class="row">
                                        <div class="col-lg-7 text-left">
                                            <div class="banner-text-left z-5" data-wow-delay=".6s" style="color: black; position:relative; top: 1%;">
                                                
                                                <div class="sub-banner-heads" style="margin-top: -30%;">
                                                    <h3 class="f-700 mb-10 fs-24 merry"><%= item.NamaEvent %></h3>
                                                </div>
                                                <a href="/Event/<%= item.RequestNumber %>" class="btn-rnd btn btn-blue">DETAIL <span><i class="fas fa-chevron-right"></i></span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <% }); %>
                    </div>
                </section>
            </div>
        <% } %>
        <% if (Resp_Sub.Count > 0 )
           {  %>
            <div class="kt-container card" >
            
            <section class="section" >
                <div class="row align-items-center mb-40 mt-20">
                    <div class="col-lg-9 text-center text-lg-left">
                        <div class="line-heads left">
                            <h1>More Upcoming Events</h1>
                        </div>
                    </div>
                <div class="col-lg-3 text-center text-lg-right">
                    <a href="/eventlist" class="btn btn-shadow mt-md-25">
                        View All
                    </a>
                </div>
            </div>
                <div class="row" style="margin-top: 20px;">
                    <% Resp_Sub.ForEach(item =>
                        { %>
                        <div class="col-md-4" >
                            <a href="/Event/<%= item.RequestNumber %>" >
                                <div class="card" style="width: 100%; ">
                                    <object data="<%= item.LinkBanner %>" type="image/png" style="width: 100%; height: 200px">
                                        <img src="/Media/images/img-broken.png" class="card-img-top" style="width: 100%; " alt="Event Poster" />  
                                    </object>
                                    <div class="card-body" style="text-align: center; background-color: #f5e8e6; height: 100px">
                                        <h5 class="card-title event-name" style="color: black;"><b><%= item.NamaEvent %></b></h5>
                                        <p class="card-text">
                                            <span><%= item.TanggalPelaksanaanDari %></span>
                                        </p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <% }); %>
                    </div>
            </section>
        </div>
        <% } %>
    </div>
    <script src="<%: ResolveUrl("~/Content/assets/Event/js/jquery-1.12.4.min.js") %>"></script>
    
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
</asp:Content>



