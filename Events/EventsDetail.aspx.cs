﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Pertamina.CORSEC._2019.Events
{
    public partial class EventsDetail : System.Web.UI.Page
    {
        public string countingDown, timeEvent;
        public GetRequestOrder.New_GetList_OperationResponse Resp_;
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Resp_ = new GetRequestOrder.New_GetList_OperationResponse();
            string orderNumber = Page.RouteData.Values["OrderNumber"] as String;
            GetRequestOrder.AuthenticationInfo User = new GetRequestOrder.AuthenticationInfo();
            User.userName = "corpsecintegration";
            User.password = "Pertamin@123";
            try
            {
                if (orderNumber != null)
                {
                    GetRequestOrder.New_Port_0PortTypeClient eventService = new GetRequestOrder.New_Port_0PortTypeClient();
                    GetRequestOrder.New_GetList_OperationRequest req = new GetRequestOrder.New_GetList_OperationRequest(User, "'Summary' = \"Request Feature Input Event Dashboard\"  AND 'Request Number'=\"" + orderNumber + "\" ", "0", "1");
                    eventService.Open();
                    var Response = eventService.New_GetList_OperationAsync(req);
                    eventService.Close();
                    try
                    {
                        this.Resp_ = Response.Result;
                        SetDateToString(Response.Result.TanggalPelaksanaanDari.ToString());
                        SetTimeStart(Response.Result.WaktuPelaksanaanDari.ToString());
                    }
                    catch (Exception error)
                    {
                        Console.WriteLine(error.Message);
                    }
                }
                else
                {
                    Response.Redirect("~/EventIndex.aspx");
                }
            }
            catch (Exception error)
            {
                Console.WriteLine(error);
            }
        }
        void SetTimeStart(String TimeEvent)
        {
            Char[] spearator = { ' ' };
            String[] strList = TimeEvent.Split(spearator);
            Char[] sparator2 = { ':' };
            String[] TimeStart = strList[1].Split(sparator2);
            if(TimeStart[0].Length != 2)
            {
                this.timeEvent = "0"+ TimeStart[0] + ":" + TimeStart[1] + " " + strList[2];
            } else
            {
                this.timeEvent = TimeStart[0] + ":" + TimeStart[1] + " " + strList[2];
            }
        }
        void SetDateToString(String DateEvent)
        {
            Char[] spearator = { '/' };
            String[] strList = DateEvent.Split(spearator);
            Char[] sparator2 = { ' ' };
            String[] Years = strList[2].Split(sparator2);

            string bulan = "";
            switch(Int32.Parse(strList[0]))
            {
                case 1:
                    bulan = "Januari";
                    break;
                case 2:
                    bulan = "Febuari";
                    break;
                case 3:
                    bulan = "Maret";
                    break;
                case 4:
                    bulan = "April";
                    break;
                case 5:
                    bulan = "Mei";
                    break;
                case 6:
                    bulan = "Juni";
                    break;
                case 7:
                    bulan = "Juli";
                    break;
                case 8:
                    bulan = "Agustus";
                    break;
                case 9:
                    bulan = "September";
                    break;
                case 10:
                    bulan = "Oktober";
                    break;
                case 11:
                    bulan = "November";
                    break;
                case 12:
                    bulan = "Desember";
                    break;
            }

            this.countingDown = strList[1] + " " + bulan + " " + Years[0];
        }
    }
}