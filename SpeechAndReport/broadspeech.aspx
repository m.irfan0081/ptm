﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CORSEC.Master" AutoEventWireup="true" CodeBehind="broadspeech.aspx.cs" Inherits="Pertamina.CORSEC._2019.SpeechAndReport.broadspeech" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">

    <!-- end:: Header -->
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <!-- begin:: Content Head -->
        <div class="kt-subheader  kt-grid__item" id="kt_subheader">
            <div class="kt-container  kt-container--fluid ">
                <div class="kt-subheader__main">
                    <div class="kt-subheader__group" id="kt_subheader_search">
                        <form class="kt-margin-l-20" id="kt_subheader_search_form">
                            <div class="kt-input-icon kt-input-icon--right kt-subheader__search">
                                <input type="text" class="form-control" placeholder="Search..." id="generalSearch">
                                <span class="kt-input-icon__icon kt-input-icon__icon--right">
                                    <span>
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                            width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                <path
                                                    d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z"
                                                    fill="#000000" fill-rule="nonzero" opacity="0.3">
                                                </path>
                                                <path
                                                    d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z"
                                                    fill="#000000" fill-rule="nonzero">
                                                </path>
                                            </g>
                                        </svg>

                                        <!--<i class="flaticon2-search-1"></i>-->
                                    </span>
                                </span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- end:: Content Head -->

        <div class="kt-container kt-pt10 kt-container--fluid  kt-grid__item kt-grid__item--fluid">

            <!--begin:: Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">
                    <div class="kt-widget kt-widget--user-profile-3">
                        <div class="kt-widget__top">
                            <div class="kt-widget__media kt-hidden-">
                                <img src="<%: ResolveUrl("~/Content/assets/media/files/doc.svg") %>" alt="image">
                            </div>
                            <div class="kt-widget__content">
                                <div class="kt-widget__head">
                                    <a href="#" class="kt-widget__username">Dokumen Pidato Manajemen
                                    </a>
                                    <div class="kt-widget__action">
                                        <button type="button" class="btn btn-brand btn-sm btn-upper">Download</button>
                                    </div>
                                </div>
                                <span>20 April 2020</span>
                                <div class="kt-widget__info">
                                    <div class="kt-widget__desc kt-mt-10">
                                        I distinguish three main text objektive could be merely to inform people.
														<br>
                                        A second could be persuade people.You want people to bay objective
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end:: Portlet-->

            <!--begin:: Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">
                    <div class="kt-widget kt-widget--user-profile-3">
                        <div class="kt-widget__top">
                            <div class="kt-widget__media kt-hidden-">
                                <img src="<%: ResolveUrl("~/Content/assets/media/files/doc.svg") %>" alt="image">
                            </div>
                            <div class="kt-widget__content">
                                <div class="kt-widget__head">
                                    <a href="#" class="kt-widget__username">Dokumen Pidato Manajemen
                                    </a>
                                    <div class="kt-widget__action">
                                        <button type="button" class="btn btn-secondary btn-sm btn-upper">Download</button>
                                    </div>
                                </div>
                                <span>24 April 2020</span>
                                <div class="kt-widget__info">
                                    <div class="kt-widget__desc kt-mt-10">
                                        I distinguish three main text objektive could be merely to inform people.
														<br>
                                        A second could be persuade people.You want people to bay objective
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end:: Portlet-->

            <!--begin:: Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">
                    <div class="kt-widget kt-widget--user-profile-3">
                        <div class="kt-widget__top">
                            <div class="kt-widget__media kt-hidden-">
                                <img src="<%: ResolveUrl("~/Content/assets/media/files/doc.svg") %>" alt="image">
                            </div>
                            <div class="kt-widget__content">
                                <div class="kt-widget__head">
                                    <a href="#" class="kt-widget__username">Dokumen Pidato Manajemen
                                    </a>
                                    <div class="kt-widget__action">
                                        <button type="button" class="btn btn-brand btn-sm btn-upper">Download</button>
                                    </div>
                                </div>
                                <span>22 April 2020</span>
                                <div class="kt-widget__info">
                                    <div class="kt-widget__desc kt-mt-10">
                                        I distinguish three main text objektive could be merely to inform people.
														<br>
                                        A second could be persuade people.You want people to bay objective
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end:: Portlet-->

            <!--begin:: Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">
                    <div class="kt-widget kt-widget--user-profile-3">
                        <div class="kt-widget__top">
                            <div class="kt-widget__media kt-hidden-">
                                <img src="<%: ResolveUrl("~/Content/assets/media/files/doc.svg") %>" alt="image">
                            </div>
                            <div class="kt-widget__content">
                                <div class="kt-widget__head">
                                    <a href="#" class="kt-widget__username">Dokumen Pidato Manajemen
                                    </a>
                                    <div class="kt-widget__action">
                                        <button type="button" class="btn btn-brand btn-sm btn-upper">Download</button>
                                    </div>
                                </div>
                                <span>05 April 2020</span>
                                <div class="kt-widget__info">
                                    <div class="kt-widget__desc kt-mt-10">
                                        I distinguish three main text objektive could be merely to inform people.
														<br>
                                        A second could be persuade people.You want people to bay objective
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end:: Portlet-->

            <!--begin:: Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__body">
                    <div class="kt-widget kt-widget--user-profile-3">
                        <div class="kt-widget__top">
                            <div class="kt-widget__media kt-hidden-">
                                <img src="<%: ResolveUrl("~/Content/assets/media/files/doc.svg") %>" alt="image">
                            </div>
                            <div class="kt-widget__content">
                                <div class="kt-widget__head">
                                    <a href="#" class="kt-widget__username">Dokumen Pidato Manajemen
                                    </a>
                                    <div class="kt-widget__action">
                                        <button type="button" class="btn btn-secondary btn-sm btn-upper">Download</button>
                                    </div>
                                </div>
                                <span>13 April 2020</span>
                                <div class="kt-widget__info">
                                    <div class="kt-widget__desc kt-mt-10">
                                        I distinguish three main text objektive could be merely to inform people.
														<br>
                                        A second could be persuade people.You want people to bay objective
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end:: Portlet-->



            <!--Begin::Pagination-->
            <div class="row">
                <div class="col-xl-12">

                    <!--begin:: Components/Pagination/Default-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__body">

                            <!--begin: Pagination-->
                            <div class="kt-pagination kt-pagination--brand">
                                <ul class="kt-pagination__links">
                                    <li class="kt-pagination__link--first">
                                        <a href="#"><i class="fa fa-angle-double-left kt-font-brand"></i></a>
                                    </li>
                                    <li class="kt-pagination__link--next">
                                        <a href="#"><i class="fa fa-angle-left kt-font-brand"></i></a>
                                    </li>
                                    <li>
                                        <a href="#">...</a>
                                    </li>
                                    <li>
                                        <a href="#">29</a>
                                    </li>
                                    <li>
                                        <a href="#">30</a>
                                    </li>
                                    <li class="kt-pagination__link--active">
                                        <a href="#">31</a>
                                    </li>
                                    <li>
                                        <a href="#">32</a>
                                    </li>
                                    <li>
                                        <a href="#">33</a>
                                    </li>
                                    <li>
                                        <a href="#">34</a>
                                    </li>
                                    <li>
                                        <a href="#">...</a>
                                    </li>
                                    <li class="kt-pagination__link--prev">
                                        <a href="#"><i class="fa fa-angle-right kt-font-brand"></i></a>
                                    </li>
                                    <li class="kt-pagination__link--last">
                                        <a href="#"><i class="fa fa-angle-double-right kt-font-brand"></i></a>
                                    </li>
                                </ul>
                                <div class="kt-pagination__toolbar">
                                    <select class="form-control kt-font-brand" style="width: 60px">
                                        <option value="10">10</option>
                                        <option value="20">20</option>
                                        <option value="30">30</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>
                                    <span class="pagination__desc">Displaying 10 of 230 records
                                    </span>
                                </div>
                            </div>

                            <!--end: Pagination-->
                        </div>
                    </div>

                    <!--end:: Components/Pagination/Default-->
                </div>
            </div>

            <!--End::Pagination-->
        </div>

    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
</asp:Content>
