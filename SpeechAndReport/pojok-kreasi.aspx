﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CORSEC.Master" AutoEventWireup="true" CodeBehind="pojok-kreasi.aspx.cs" Inherits="Pertamina.CORSEC._2019.SpeechAndReport.pojok_kreasi" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">

    <!-- end:: Header -->
    <div class="kt-grid kt-grid--hor" id="kt_content_header">

        <!-- begin:: Content Head -->
        <div class="kt-subheader  kt-grid__item" id="kt_subheader">
            <div class="kt-container  kt-container--fluid ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">Dashboard</h3>
                    <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                    <a href="#" class="btn btn-label-success btn-bold btn-sm btn-icon-h kt-margin-l-10">
                        <i class="fa fa-plus"></i>Tambah Data
                    </a>
                    <div class="kt-input-icon kt-input-icon--right kt-subheader__search kt-hidden">
                        <input type="text" class="form-control" placeholder="Search order..." id="generalSearch">
                        <span class="kt-input-icon__icon kt-input-icon__icon--right">
                            <span><i class="flaticon2-search-1"></i></span>
                        </span>
                    </div>
                </div>
                <div class="kt-subheader__toolbar">
                    <div class="kt-subheader__wrapper">
                        <a href="#" class="btn kt-subheader__btn-daterange" id="kt_dashboard_daterangepicker" data-toggle="kt-tooltip" title="Select dashboard daterange" data-placement="left">
                            <span class="kt-subheader__btn-daterange-title" id="kt_dashboard_daterangepicker_title">Today</span>&nbsp;
											<span class="kt-subheader__btn-daterange-date" id="kt_dashboard_daterangepicker_date">Aug 16</span>
                            <i class="flaticon2-calendar-1"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- end:: Content Head -->

    <!-- begin:: Content -->
    <div class="kt-content kt-content--fit-top  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <!-- begin:: Content -->

        <div class="kt-container kt-pt10">
            <div class="kt-portlet">
                <div class="kt-portlet__body">
                    <h2 class="kt-infobox__title kt-mb-40">Pojok Kreasi</h2>
                    <div class="row kt-mb-50">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-4">
                                    <img src="<%: ResolveUrl("~/Content/assets/media/products/product1.jpg") %>" class="img-responsive" alt="Image" height="90">
                                </div>
                                <div class="col-md-8">
                                    <h4>Dipisicing Ipsum, ducimus.</h4>
                                    <p class="">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quisquam mollitia reiciendis eveniet repudiandae temporibus totam</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-4">
                                    <img src="<%: ResolveUrl("~/Content/assets/media/products/product2.jpg") %>" class="img-responsive" alt="Image" height="90">
                                </div>
                                <div class="col-md-8">
                                    <h4>Dipisicing Ipsum, ducimus.</h4>
                                    <p class="">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quisquam mollitia reiciendis eveniet repudiandae temporibus totam</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row kt-mb-50">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-4">
                                    <img src="<%: ResolveUrl("~/Content/assets/media/products/product3.jpg") %>" class="img-responsive" alt="Image" height="90">
                                </div>
                                <div class="col-md-8">
                                    <h4>Dipisicing Ipsum, ducimus.</h4>
                                    <p class="">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quisquam mollitia reiciendis eveniet repudiandae temporibus totam</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-4">
                                    <img src="<%: ResolveUrl("~/Content/assets/media/products/product4.jpg") %>" class="img-responsive" alt="Image" height="90">
                                </div>
                                <div class="col-md-8">
                                    <h4>Dipisicing Ipsum, ducimus.</h4>
                                    <p class="">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quisquam mollitia reiciendis eveniet repudiandae temporibus totam</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row kt-mb-50">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-4">
                                    <img src="<%: ResolveUrl("~/Content/assets/media/products/product5.jpg") %>" class="img-responsive" alt="Image" height="90">
                                </div>
                                <div class="col-md-8">
                                    <h4>Dipisicing Ipsum, ducimus.</h4>
                                    <p class="">
                                        Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quisquam mollitia
														reiciendis eveniet repudiandae temporibus totam
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-4">
                                    <img src="<%: ResolveUrl("~/Content/assets/media/products/product6.jpg") %>" class="img-responsive" alt="Image" height="90">
                                </div>
                                <div class="col-md-8">
                                    <h4>Dipisicing Ipsum, ducimus.</h4>
                                    <p class="">
                                        Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quisquam mollitia
														reiciendis eveniet repudiandae temporibus totam
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- end:: Section -->

        <!-- end:: Content -->
    </div>
    <!-- end:: Content -->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
</asp:Content>
