﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CORSEC.Master" AutoEventWireup="true" CodeBehind="presentasi.aspx.cs" Inherits="Pertamina.CORSEC._2019.SpeechAndReport.presentasi" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <!-- end:: Header -->
    <div class="kt-grid kt-grid--hor" id="kt_content">

        <!-- begin:: Content Head -->
        <div class="kt-subheader  kt-grid__item" id="kt_subheader">
            <div class="kt-container  kt-container--fluid ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">Dashboard</h3>
                    <span class="kt-subheader__separator kt-subheader__separator--v"></span>
                    <a href="#" class="btn btn-label-success btn-bold btn-sm btn-icon-h kt-margin-l-10">
                        <i class="fa fa-plus"></i>Tambah Data
                    </a>
                    <div class="kt-input-icon kt-input-icon--right kt-subheader__search kt-hidden">
                        <input type="text" class="form-control" placeholder="Search order..." id="generalSearch">
                        <span class="kt-input-icon__icon kt-input-icon__icon--right">
                            <span><i class="flaticon2-search-1"></i></span>
                        </span>
                    </div>
                </div>
                <div class="kt-subheader__toolbar">
                    <div class="kt-subheader__wrapper">
                        <a href="#" class="btn kt-subheader__btn-daterange" id="kt_dashboard_daterangepicker" data-toggle="kt-tooltip" title="Select dashboard daterange" data-placement="left">
                            <span class="kt-subheader__btn-daterange-title" id="kt_dashboard_daterangepicker_title">Today</span>&nbsp;
											<span class="kt-subheader__btn-daterange-date" id="kt_dashboard_daterangepicker_date">Aug 16</span>
                            <i class="flaticon2-calendar-1"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- end:: Content Head -->

    <!-- begin:: Content -->
    <div class="kt-container kt-mt-10">

        <!--Begin::App-->
        <div class="kt-grid kt-grid--desktop kt-grid--ver kt-grid--ver-desktop kt-app">

            <!--Begin:: App Aside-->
            <div class="kt-grid__item kt-app__toggle" id="kt_user_profile_aside">

                <!--begin:: Widgets/Applications/User/Profile4-->
                <div class="kt-portlet kt-portlet--height-fluid-">
                    <div class="kt-portlet__body">

                        <!--begin::Widget -->
                        <div class="kt-widget kt-widget--user-profile-4">
                            <div class="kt-widget__body mg-0">
                                <div class="accordion accordion-solid kt-mb-5" id="accordionExample6">
                                    <div class="card">
                                        <div class="card-header" id="headingOne6">
                                            <div class="card-title" data-toggle="collapse" data-target="#collapseOne6" aria-expanded="true"
                                                aria-controls="collapseOne6">
                                                <i class="flaticon-doc"></i>Pesentasi Umum
                                            </div>
                                        </div>
                                        <div id="collapseOne6" class="collapse show" aria-labelledby="headingOne6"
                                            data-parent="#accordionExample6">
                                            <div class="card-body">
                                                <a href="#" class="kt-widget__item kt-pl20 kt-pr20 kt-widget__item--active">Tahun 2011
                                                </a>
                                                <a href="#" class="kt-widget__item kt-pl20 kt-pr20">Tahun 2012
                                                </a>
                                                <a href="#" class="kt-widget__item kt-pl20 kt-pr20">Tahun 2013
                                                </a>
                                                <a href="#" class="kt-widget__item kt-pl20 kt-pr20">Tahun 2014
                                                </a>
                                                <a href="#" class="kt-widget__item kt-pl20 kt-pr20">Tahun 2015
                                                </a>
                                                <a href="#" class="kt-widget__item kt-pl20 kt-pr20">Tahun 2016
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header" id="headingTwo6">
                                            <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseTwo6"
                                                aria-expanded="false" aria-controls="collapseTwo6">
                                                <i class="flaticon-doc"></i>Presentasi Terbatas
                                            </div>
                                        </div>
                                        <div id="collapseTwo6" class="collapse" aria-labelledby="headingTwo6"
                                            data-parent="#accordionExample6">
                                            <div class="card-body">
                                                <a href="#" class="kt-widget__item kt-pl20 kt-pr20">Tahun 2011
                                                </a>
                                                <a href="#" class="kt-widget__item kt-pl20 kt-pr20">Tahun 2012
                                                </a>
                                                <a href="#" class="kt-widget__item kt-pl20 kt-pr20">Tahun 2013
                                                </a>
                                                <a href="#" class="kt-widget__item kt-pl20 kt-pr20">Tahun 2014
                                                </a>
                                                <a href="#" class="kt-widget__item kt-pl20 kt-pr20">Tahun 2015
                                                </a>
                                                <a href="#" class="kt-widget__item kt-pl20 kt-pr20">Tahun 2016
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--end::Widget -->
                    </div>
                </div>

                <!--end:: Widgets/Applications/User/Profile4-->

            </div>

            <!--End:: App Aside-->

            <!--Begin:: App Content-->
            <div class="kt-grid__item kt-grid__item--fluid kt-app__content kt-ml-10">
                <div class="row">
                    <div class="col-xl-6">

                        <!--begin:: Widgets/Last Updates-->
                        <div class="kt-portlet kt-portlet--height-fluid">
                            <div class="kt-portlet__head">
                                <div class="kt-portlet__head-label">
                                    <h3 class="kt-portlet__head-title">Gallery Pertapedia
                                    </h3>
                                </div>
                                <div class="kt-portlet__head-toolbar">
                                    <div class="dropdown-menu dropdown-menu-fit dropdown-menu-md dropdown-menu-right">

                                        <!--begin::Nav-->
                                        <ul class="kt-nav">
                                            <li class="kt-nav__head">Export Options
																	<span data-toggle="kt-tooltip" data-placement="right" title=""
                                                                        data-original-title="Click to learn more...">
                                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                                            xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                                                            height="24px" viewBox="0 0 24 24" version="1.1"
                                                                            class="kt-svg-icon kt-svg-icon--brand kt-svg-icon--md1">
                                                                            <g stroke="none" stroke-width="1" fill="none"
                                                                                fill-rule="evenodd">
                                                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                                                <circle fill="#000000" opacity="0.3" cx="12" cy="12"
                                                                                    r="10">
                                                                                </circle>
                                                                                <rect fill="#000000" x="11" y="10" width="2" height="7"
                                                                                    rx="1">
                                                                                </rect>
                                                                                <rect fill="#000000" x="11" y="7" width="2" height="2"
                                                                                    rx="1">
                                                                                </rect>
                                                                            </g>
                                                                        </svg>
                                                                    </span>
                                            </li>
                                            <li class="kt-nav__separator"></li>
                                            <li class="kt-nav__item">
                                                <a href="#" class="kt-nav__link">
                                                    <i class="kt-nav__link-icon flaticon2-drop"></i>
                                                    <span class="kt-nav__link-text">Activity</span>
                                                </a>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a href="#" class="kt-nav__link">
                                                    <i class="kt-nav__link-icon flaticon2-calendar-8"></i>
                                                    <span class="kt-nav__link-text">FAQ</span>
                                                </a>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a href="#" class="kt-nav__link">
                                                    <i class="kt-nav__link-icon flaticon2-telegram-logo"></i>
                                                    <span class="kt-nav__link-text">Settings</span>
                                                </a>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a href="#" class="kt-nav__link">
                                                    <i class="kt-nav__link-icon flaticon2-new-email"></i>
                                                    <span class="kt-nav__link-text">Support</span>
                                                    <span class="kt-nav__link-badge">
                                                        <span
                                                            class="kt-badge kt-badge--success kt-badge--rounded">5</span>
                                                    </span>
                                                </a>
                                            </li>
                                            <li class="kt-nav__separator"></li>
                                            <li class="kt-nav__foot">
                                                <a class="btn btn-label-danger btn-bold btn-sm" href="#">Upgrade
																		plan</a>
                                                <a class="btn btn-clean btn-bold btn-sm" href="#"
                                                    data-toggle="kt-tooltip" data-placement="right" title=""
                                                    data-original-title="Click to learn more...">Learn more</a>
                                            </li>
                                        </ul>

                                        <!--end::Nav-->
                                    </div>
                                </div>
                            </div>
                            <div class="kt-portlet__body">

                                <!--begin::widget 12-->
                                <div class="kt-widget4">
                                    <div class="kt-widget4__item">
                                        <img class="kt-mr-10" src="<%: ResolveUrl("~/Content/assets/media/files/pdf.svg") %>" height="26" alt="">
                                        <a href="#" class="kt-widget4__title kt-widget4__title--light">Documents v6 has been arrived!
                                        </a>
                                        <small class="kt-widget4__number fsize-11 kt-mr-20">PDF - 23mb</small>
                                        <span class="kt-widget3__number kt-font-info">
                                            <a href="#" class="btn-label-brand btn btn-sm btn-bold">Download</a>
                                        </span>
                                    </div>
                                    <div class="kt-widget4__item">
                                        <img class="kt-mr-10" src="<%: ResolveUrl("~/Content/assets/media/files/doc.svg") %>" height="26" alt="">
                                        <a href="#" class="kt-widget4__title kt-widget4__title--light">Documents community meet-up 2019 in Rome.
                                        </a>
                                        <small class="kt-widget4__number fsize-11 kt-mr-20">PDF - 23mb</small>
                                        <span class="kt-widget3__number kt-font-info">
                                            <a href="#" class="btn-label-brand btn btn-sm btn-bold">Download</a>
                                        </span>
                                    </div>
                                    <div class="kt-widget4__item">
                                        <img class="kt-mr-10" src="<%: ResolveUrl("~/Content/assets/media/files/jpg.svg") %>" height="26" alt="">
                                        <a href="#" class="kt-widget4__title kt-widget4__title--light">Documents Angular 8 version will be landing soon...
                                        </a>
                                        <small class="kt-widget4__number fsize-11 kt-mr-20">PDF - 23mb</small>
                                        <span class="kt-widget3__number kt-font-info">
                                            <a href="#" class="btn-label-brand btn btn-sm btn-bold">Download</a>
                                        </span>
                                    </div>
                                    <div class="kt-widget4__item">
                                        <img class="kt-mr-10" src="<%: ResolveUrl("~/Content/assets/media/files/doc.svg") %>" height="26" alt="">
                                        <a href="#" class="kt-widget4__title kt-widget4__title--light">ale! Purchase Documents at 70% off for limited time
                                        </a>
                                        <small class="kt-widget4__number fsize-11 kt-mr-20">PDF - 23mb</small>
                                        <span class="kt-widget3__number kt-font-info">
                                            <a href="#" class="btn-label-brand btn btn-sm btn-bold">Download</a>
                                        </span>
                                    </div>
                                    <div class="kt-widget4__item">
                                        <img class="kt-mr-10" src="<%: ResolveUrl("~/Content/assets/media/files/pdf.svg") %>" height="26" alt="">
                                        <a href="#" class="kt-widget4__title kt-widget4__title--light">Documents VueJS version is in progress. Stay tuned!
                                        </a>
                                        <small class="kt-widget4__number fsize-11 kt-mr-20">Excel - 23mb</small>
                                        <span class="kt-widget3__number kt-font-info">
                                            <a href="#" class="btn-label-brand btn btn-sm btn-bold">Download</a>
                                        </span>
                                    </div>
                                    <div class="kt-widget4__item">
                                        <img class="kt-mr-10" src="<%: ResolveUrl("~/Content/assets/media/files/doc.svg") %>" height="26" alt="">
                                        <a href="#" class="kt-widget4__title kt-widget4__title--light">Black Friday! Purchase Documents at ever lowest 90% off for limited time
                                        </a>
                                        <small class="kt-widget4__number fsize-11 kt-mr-20">PDF - 23mb</small>
                                        <span class="kt-widget3__number kt-font-info">
                                            <a href="#" class="btn-label-brand btn btn-sm btn-bold">Download</a>
                                        </span>
                                    </div>
                                    <div class="kt-widget4__item">
                                        <img class="kt-mr-10" src="<%: ResolveUrl("~/Content/assets/media/files/pdf.svg") %>" height="26" alt="">
                                        <a href="#" class="kt-widget4__title kt-widget4__title--light">Documents React version is in progress.
                                        </a>
                                        <small class="kt-widget4__number fsize-11 kt-mr-20">Word - 23mb</small>
                                        <span class="kt-widget3__number kt-font-info">
                                            <a href="#" class="btn-label-brand btn btn-sm btn-bold">Download</a>
                                        </span>
                                    </div>
                                </div>

                                <!--end::Widget 12-->
                                <div class="kt-portlet kt-mt-40">
                                    <div class="kt-portlet__body">

                                        <!--begin: Pagination-->
                                        <div class="kt-pagination kt-pagination--brand">
                                            <ul class="kt-pagination__links">
                                                <li class="kt-pagination__link--first">
                                                    <a href="#"><i class="fa fa-angle-double-left kt-font-brand"></i></a>
                                                </li>
                                                <li class="kt-pagination__link--next">
                                                    <a href="#"><i class="fa fa-angle-left kt-font-brand"></i></a>
                                                </li>
                                                <li>
                                                    <a href="#">...</a>
                                                </li>
                                                <li>
                                                    <a href="#">29</a>
                                                </li>
                                                <li>
                                                    <a href="#">30</a>
                                                </li>
                                                <li class="kt-pagination__link--active">
                                                    <a href="#">31</a>
                                                </li>
                                                <li>
                                                    <a href="#">32</a>
                                                </li>
                                                <li>
                                                    <a href="#">33</a>
                                                </li>
                                                <li>
                                                    <a href="#">34</a>
                                                </li>
                                                <li>
                                                    <a href="#">...</a>
                                                </li>
                                                <li class="kt-pagination__link--prev">
                                                    <a href="#"><i class="fa fa-angle-right kt-font-brand"></i></a>
                                                </li>
                                                <li class="kt-pagination__link--last">
                                                    <a href="#"><i class="fa fa-angle-double-right kt-font-brand"></i></a>
                                                </li>
                                            </ul>
                                        </div>

                                        <!--end: Pagination-->
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!--end:: Widgets/Last Updates-->
                    </div>
                </div>
            </div>

            <!--End:: App Content-->
        </div>

        <!--End::App-->
    </div>
    <!-- end:: Content -->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
</asp:Content>
